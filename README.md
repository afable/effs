
Erik Afable

Last updated: January 4, 2012

----------------------------------------
Description:
----------------------------------------

The Extremely Fast File System (EFFS) simulates the file system of a hard disk. The lower layer driver of the file system is "fisk.h". "effs.c" consists of the main code base of the file system and accesses the "fisk.h" API. The test harness "effs_test.c" and interactive shell "shell.c" sit above "effs.c" and access its API. EFFS is based on the old Unix File System that uses 12 direct pointers to data and then single, double, and triple levels of indirection. EFFS consists of a superblock, inode space, and data space.

----------------------------------------
Simplifying Assumptions:
----------------------------------------

	INITIALIZATION:
		We have only one fisk of a predetermined size FISK_SIZE.
		We cover two cases for a fisk initialization:
			1. Fisk does not exists and is created of size
				FISK_SIZE*1024 Bytes.
			2. Fisk already exists and is of size FISK_SIZE*1024
				Bytes.

	DIRECTORIES
		Directories are a fixed size of 1 block (1024 Bytes).
		Directories can have at most 48 files contained within
		 them (48 sub-directories and "." and "..")

----------------------------------------
Usage:
----------------------------------------

	Test Harness	
	To run the test harness, simply make the build and run the effs_test executable.

	Shell
	To run the interactive shell, change the Makefile to output the effs_shell executable like so:

		#effs_test: effs.o fisk.o effs_test.o
		#	gcc -g -Wall effs.o fisk.o effs_test.o -o effs_test

		effs_shell: effs.o fisk.o shell.o
			gcc -g -Wall effs.o fisk.o shell.o -o effs_shell

	In the interactive shell, you can create files, create directories, and read or write bits as ints or chars on the fisk directly. A list of usage commands are listed below:

		$ ls
		$ pwd
		$ cd <path>
		$ mkdir <dirname>
		$ mkfile <filename> <filesize>
		$ rm <filename>
		$ open <filename>
		$ write <filename> <content> <filesize>
			- a buffer full of 'a' characters will be written to that block location
		$ read <filename> <filesize> <mode>
			- mode: 1 for char, 2 for int
		$ test <startblock> <endblock> <mode>
			- print actual values on fisk from startblock to endblock
			- mode: 1 for char, 2 for int
			- the INODE OFFSET and DATA OFFSET show which blocks inode space and data space start







