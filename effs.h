/*
 * File: effs.h
 * Description: The Extremely Fast File System (EFFS), effs.c simulates
 *		a file system with a file, i.e. a "fisk", as disk and separate
 *		blocks of the file as "fisk files". This file system is based on
 *		the older Unix File System consisting of superblock, inode, data,
 *		boot, and swap spaces. The effs.h API is also used by the test harness
 *		effs_test.c and the interactive shell effs_test.c. For their uses, please
 *		refer to the README file.
 * Coders: Erik Afable, Jaime Lai, James Xiang
 * Code Design Contributors: Nazma Panjwani
 * Report Created By: Jaime Lai, Nazma Panjwani
 *
 * Last updated: December 2, 2011
 *
 * Simplifying assumptions:
 *		-memory leaks not tested
 *		-boot and swap space not implemented
 */

#include "fisk.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

/* defines */
#define OCCUPIED 1
#define FREE 0
#define DIRECTORY 1
#define FILE 0
#define EFFS_ROOT_CHECKSUM 1337
#define INODE_SIZE 18
#define MAX_FILECOUNT 48

// interface functions
int effs_initialize(int numblocks);
int effs_create(char* name, int flag, int filesize);
int effs_delete(char* filename);
int effs_open(char* filename);
int effs_write(int inode_index, void *file_content, int filesize);
int effs_read(char* filename, void *file_content, int filesize);
int effs_sync(int fildes);
int effs_close(int fildes);

// additional functions for shell
int create_file(int filesize, char* filename);
int create_dir(char* dirname);
void print_curr_dir();
void change_dir(char* dirname);
int get_curr_dir();


