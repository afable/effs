/*
 * File: fisk.c
 * Description: The fisk.h API simulates hard disk drivers used by the
 * 		Extremely Fast File System (EFFS), effs.c.
 * Coders: Erik Afable, Jaime Lai, James Xiang
 * Code Design Contributors: Nazma Panjwani
 * Report Created By: Jaime Lai, Nazma Panjwani
 *
 * Last updated: November 4, 2011
 */

#include "fisk.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#define BLOCK_SIZE 1024

static int fd;

/* opens the "fisk" for reading/writing */
int openDisk(char *filename, int nbytes)
{	
	// opens the fisk; if it does not exist, a new one is created
	fd = open(filename, O_RDWR | O_CREAT, (mode_t)0600);
	if (fd == -1) {
		perror("Error opening file for writing");
        return -1;
    }
	
    // ensures the file is of size nbytes 
	lseek(fd, nbytes-1, SEEK_SET);
	write(fd, "\0", 1);
	
	// returns the file descriptor
	return fd;
}

/* closes the "fisk" */
int closeDisk()
{
	int result;
	result = close(fd);
	
	if (result == -1) {
		perror("Error closing file");
		return -1;
	}
	
	return 0;
}

/* reads the block specified by blocknr and places the contents 
into the buffer block */
int readBlock(int disk, int blocknr, void *block)
{
	int result;
	
	// sets the file pointer to position specified by blocknr
	result = lseek(disk, blocknr*BLOCK_SIZE, SEEK_SET);
	
	if(result == -1) {
		perror("Error calling lseek() to set starting position in reading");
	}
	
	// obtains the contents of the block and places it in the block buffer
	result = read(disk, block, BLOCK_SIZE);
	
	if(result == -1) {
		perror("Error reading from disk");
	}
	
	return 0;
}

/* writes the contents of a single block from the buffer *block into the fisk block disk specified by the blocknr */
int writeBlock(int disk, int blocknr, void *block)
{
	int result;
	
	// sets the file pointer to position specified by blocknr
	result = lseek(disk, blocknr*BLOCK_SIZE, SEEK_SET);
	
	if(result == -1){
		perror("Error calling lseek() to set starting position in writing");
	}
	
	// writes the contents of the block buffer into the "fisk" block	
	result = write(disk, block, BLOCK_SIZE);
	
	if(result == -1){
		perror("Error writing to disk");
	}
	
	return 0;
}

/* forces any outstanding writes to the "fisk" immediately */
void syncDisk()
{
	int result;
	
	// syncs the "fisk"
	result = fsync(fd);
       
	if(result == -1){
		perror("Error synchronizing to disk");
	}
}


