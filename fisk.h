/*
 * File: fisk.h
 * Description: The fisk.h API simulates hard disk drivers used by the
 * 		Extremely Fast File System (EFFS), effs.c.
 * Coders: Erik Afable, Jaime Lai, James Xiang
 * Code Design Contributors: Nazma Panjwani
 * Report Created By: Jaime Lai, Nazma Panjwani
 *
 * Last updated: November 4, 2011
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#define BLOCK_SIZE 1024

int openDisk(char *filename, int nbytes);

int closeDisk();

int readBlock(int disk, int blocknr, void *block);

int writeBlock(int disk, int blocknr, void *block);

void syncDisk();
